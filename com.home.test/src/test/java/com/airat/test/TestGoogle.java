/**
 * 
 */
package com.airat.test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestGoogle {
	static WebDriver driver;
	@BeforeClass
	public static void openBrowser() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\asadu\\Downloads\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.google.com");
	}
	
	@Test
	public void test1() {
		String currentTitle = driver.getTitle();
		Assert.assertEquals(currentTitle, "Google");
	}
	
	@Test
	public void test2() {
		boolean current = driver.findElement(By.id("hplogo")).isDisplayed();
		Assert.assertEquals(current, true);
	}
	
	@AfterClass
	public static void afterClass() {
		driver.close();
		driver.quit();
	}
}
