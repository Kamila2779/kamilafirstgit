package JunitMavenKamila.JuntTest;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class testJunit1 {
	
	@BeforeClass
	public static void RunOnceBeforeClass (){
		System.out.println("@BeforeClass - run once before class");
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\asadu\\Downloads\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			driver.get("https://www.google.com");
	}
	
	@AfterClass
	public static void RunOnceAfterClass() {
		System.out.println("@AfterClass - run once after class");
		driver.close();
		driver.quit();
				
	}
	
	@Before
	public void BeforeTestMethod() {// why not static?
		System.out.println("@Before - run before test method");
	}
	
	@Test
	public static void TestMethod1() {
		System.out.println("@Test - test method 1");
		String TitleExpected = "Google";
		System.out.println("TITLE =====>>>>>" + driver.getTitle());
		Assert.assertEquals(TitleExpected, driver.getTitle());
	}
	
	@Test
	public static void TestMethod2 () {
		System.out.println("@Test - test method 2");
		boolean current = driver.findElement(By.xpath("//img[@alt=\"Google\"")).isDisplayed();
		Assert.assertEquals(current, true);
	}
	
	@After
	public static void AfterTestMethod() {
		System.out.println("@After - run after test method");
	}

}
