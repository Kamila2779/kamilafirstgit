package listConcept;

import java.util.LinkedList;

import javax.swing.text.html.HTMLDocument.Iterator;

public class LinkedLIstConcept {

	public static void main(String[] args) {
		
		LinkedList<String> ll = new LinkedList<String>();
		
		ll.add("test");
		ll.add("qtp");
		ll.add("selenium");
		ll.add("RPA");
		ll.add("RFT");
		
		System.out.println("content of linkedlist:"+ll);
		
		ll.addFirst("Kamila");
		ll.addLast("Automation");
		
		System.out.println("content of linkedlist:"+ll);
		
		
		System.out.println(ll.get(0));
		
		ll.set(0, "Tom");
		System.out.println(ll.get(0));
		
		ll.removeFirst();
		ll.removeLast();
		System.out.println("content of linkedlist:"+ll);
		
		ll.remove(2);
		
		
		//how to print all the values of LinkedList:
		//1. forloop
		
		System.out.println("***using for loop");
		for(int i=0; i<ll.size(); i++) {
			System.out.println(ll.get(i));
		}
		
		//2. advance for loop:
		
		System.out.println("***using advance for loop");
		for (String str : ll) {
			System.out.println(str);
		}
		
		//3. iterator
		System.out.println("using iterator");
		//Iterator<String> it = ll.iterator();
		//while(it.hasNext()) {
		//	System.out.println(it.next());
	//	}
		
		//4.while loop
		System.out.println("using while loop");
		int num=0;
		while(ll.size()>num) {
			System.out.println(ll.get(num));
			num++;
			
		}
		

	}

}
