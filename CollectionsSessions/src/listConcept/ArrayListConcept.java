package listConcept;

import java.util.ArrayList;

import javax.swing.text.html.HTMLDocument.Iterator;

public class ArrayListConcept {

	public static void main(String[] args) {
		
	ArrayList ar = new ArrayList();
	
	ar.add(10);
	ar.add(20);
	ar.add(30);
	
	System.out.println(ar.size());
	
	ar.add(40);
	ar.add(50);
	ar.add(50);
	ar.add(12.33);
	ar.add("Test");
	ar.add('a');
	ar.add(true);
	
	System.out.println(ar.size());
	
	System.out.println(ar.get(4));
	
	for(int i=0; i<ar.size(); i++) {
		System.out.println(ar.get(i));
		
		}
	
	
	ArrayList<Integer> ar1 = new ArrayList<Integer>();
	ar1.add(100);
	
	ArrayList<String> ar2 = new ArrayList<String>();
	ar2.add("test");
	ar2.add("selenium");
	
	//Employee class Objects:
	Employee e1 = new Employee("Kamila", 40, "QA");
	Employee e2 = new Employee("Airat", 41, "QA");
	Employee e3 = new Employee("Malik", 5, "K");
	Employee e4 = new Employee("Ailin", 2, "P");
	
	//create arrayList:
	ArrayList<Employee> ar4 = new ArrayList<Employee>();
	ar4.add(e1);
	ar4.add(e2);
	ar4.add(e3);
	ar4.add(e4);
	
	//iterator to traverse the values:
	java.util.Iterator<Employee> it = ar4.iterator();
	while (it.hasNext()) {
		it.next();
		Employee emp = it.next();
		System.out.println(emp.name);
		System.out.println(emp.age);
		System.out.println(emp.dept);
	}
	
	ArrayList<String> ar5 = new ArrayList<String>();
	ar5.add("test");
	ar5.add("selenium");
	ar5.add("QTP");
	
	ArrayList<String> ar6 = new ArrayList<String>();
	ar6.add("dev");
	ar6.add("java");
	ar6.add("javaScript");
	
	ar5.addAll(ar6);
	for(int i=0; i<ar5.size(); i++) {
		System.out.println(ar5.get(i));
	}
	
	//removeAll:
	ar5.removeAll(ar6);
	for(int i=0; i<ar5.size(); i++) {
		System.out.println(ar5.get(i));
	}
	
	//retainAll()
	
	ArrayList<String> ar7 = new ArrayList<String>();
	ar7.add("test");
	ar7.add("selenium");
	ar7.add("QTP");
	
	ArrayList<String> ar8 = new ArrayList<String>();
	ar8.add("test");
	ar8.add("java");
	
	ar7.retainAll(ar8);
	for(int i=0; i<ar7.size(); i++) {
		System.out.println(ar7.get(i));
	}
	
	


	}

}
