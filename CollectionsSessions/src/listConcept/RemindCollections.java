package listConcept;

import java.util.HashMap;
import java.util.Map.Entry;

public class RemindCollections {

	public static void main(String[] args) {
		
		HashMap<Integer, String> family = new HashMap<Integer, String>();
		family.put(1, "Kamila");
		family.put(2, "Airat");
		family.put(3, "Malik");
		family.put(4, "Ailin");
		
		for (Entry m : family.entrySet()) {
			System.out.println(m.getKey()+ " "+ m.getValue());
		}
		
		System.out.println(family);
		
		HashMap<Integer, Employee> emp = new HashMap<Integer, Employee>();
		Employee e1 = new Employee("Mary", 25, "QA");
		Employee e2 = new Employee("Bob", 26, "Dev");
		Employee e3 = new Employee("Roy", 27, "QA");
		Employee e4 = new Employee("Steart", 28, "Admin");
		
		emp.put(1, e1);
		emp.put(2, e2);
		emp.put(3, e3);
		emp.put(4, e4);
		
		
		for (Entry<Integer, Employee> m: emp.entrySet()) {
			int key = m.getKey();
			Employee e = m.getValue();
			System.out.println("Employee "+ key + e.name+"/"+e.age +"/"+e.dept+" ");
		}
		
		

	}

}
