package com.kamila.test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import junit.framework.Assert;

public class CalculatorTest {
	static WebDriver driver;
	@BeforeClass
	public static void openBrowser() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\asadu\\Downloads\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.calculator.com/");
	}
	@AfterClass
	public static void closeBrowser() {
		driver.close();
		driver.quit();
	}
	
	@Before
	public void setZero() {
		driver.findElement(By.xpath("//input[@type='button' and @value='AC' and @class= 'button red bigroundedlong caps2']")).click();
	}
	@After
	public void setZeroAfter() {
		driver.findElement(By.xpath("//input[@type='button' and @value='AC' and @class= 'button red bigroundedlong caps2']")).click();
	}
	@Test
	public void testSum(){
		String astring = driver.findElement(By.xpath("//input[@type='button' and @value='8']")).getCssValue(propertyName);
		int a = Integer.parseInt(astring);
		String bstring = driver.findElement(By.xpath("//input[@type='button' and @value='2']")).getText();
		int b = Integer.parseInt(bstring);
		int sum = a+b;
		Assert.assertEquals(10, sum);
		System.out.println("a+b="+sum);
	}
	@Test
	public void testMult() {
		String astring = driver.findElement(By.xpath("//input[@type='button' and @value='8']")).getText();
		int a = Integer.parseInt(astring);
		String bstring = driver.findElement(By.xpath("//input[@type='button' and @value='2']")).getText();
		int b = Integer.parseInt(bstring);
		int mult = a*b;
		Assert.assertEquals(100, mult);
		System.out.println("a*b=mult");
	}
			
	

}
