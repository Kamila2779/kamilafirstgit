package com.kamila.test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestAmazon {
	static WebDriver driver;
	@BeforeClass
	public static void openBrowser(){
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\asadu\\Downloads\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://www.amazon.com");
	}
	
	@Test
	public void test1() {
		String currentTitle = driver.getTitle();
		System.out.println(currentTitle);
		Assert.assertEquals(currentTitle, "Amazon");
		
	}
	
	@Test
	public void test2() {
		boolean current = driver.findElement(By.id("nav-hamburger-menu")).isDisplayed();
		Assert.assertEquals(current, true);
		System.out.println("Test 2 done");
	}
	
	@AfterClass
	public static void closeDriver() {
		driver.close();
		driver.quit();
	}

}
