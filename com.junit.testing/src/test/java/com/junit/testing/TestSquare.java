package com.junit.testing;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class TestSquare {
	
	@Before
	public  void before() {
		System.out.println("@BeforeClass=====>start");
	}
	
	@After
	public void after() {
		System.out.println("@AfterClass======>finish");
	}
	
	@Test
	public void testSquare() {
		TestJunit test = new TestJunit();
		int output = test.square(4);
		System.out.println(output);
		Assert.assertEquals(16, output);
	}

	@Test
		public void testA() {
			TestJunit test = new TestJunit();
			int output = test.countA("Amazon");
			System.out.println("numbers of A/a ====="+output);
			Assert.assertEquals(1, output);
			//System.out.println("numbers of A/a ====="+output);
	}
}