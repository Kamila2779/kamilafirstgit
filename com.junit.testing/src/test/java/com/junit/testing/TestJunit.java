package com.junit.testing;

public class TestJunit {
	
	public int square(int x) {
		return x*x;
	}
	
	public int countA (String strawberry) {
		int count=0;
		for(int i=0; i<strawberry.length(); i++) {
			if(strawberry.charAt(i)=='a' || strawberry.charAt(i)=='A') {
				count++;
			}
		}
		return count;
	}

}
