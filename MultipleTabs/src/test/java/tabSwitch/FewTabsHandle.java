package tabSwitch;

import net.bytebuddy.implementation.bytecode.assign.TypeCasting;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class FewTabsHandle {

    @Test
    public void tabs () throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "/home/airat/PNT/Soft/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("http://mango.com");
        ((JavascriptExecutor)driver).executeScript("window.open()");
        Set<String> set = driver.getWindowHandles();
        Iterator<String> iterator = set.iterator();
        String firstWindow = iterator.next();
        driver.switchTo().window(firstWindow);
        System.out.println("first Window ID=="+firstWindow+driver.getTitle());

        String secondWindow = iterator.next();
        driver.switchTo().window(secondWindow);
        driver.get("http://google.com");
        System.out.println("second Window ID=="+secondWindow+driver.getTitle());

        driver.close();
        driver.close();



//        if(iterator.hasNext()){
//            System.out.println();
//            driver.switchTo().window(iterator.next().toString());
//            if (driver.getTitle()=="Google"){
//                Thread.sleep(3000);
//                driver.get("https://facebook.com");
//            }

        }







    }

