$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/Features.feature");
formatter.feature({
  "name": "test IFF website",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Test Home Page",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to \"https://iff.com\"",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User is on home page the title is \"International Flavors \u0026 Fragrances\"",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User is on home page the logo with text \"IFF\" is displayed in the top left corner",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User is on home page the button \"Taste\" is enabled",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User is on home page the button \"Touch\" is enabled",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User is on home page the button \"Scent\" is enabled",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User is on home page the button \"Discover\" is enabl",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User is on home page the button \"Our purpose\" is enableded",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "close and quit browser",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Test Taste Page",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to \"https://iff.com\"",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User push the button \"Taste\" the page with title \"Taste – International Flavors \u0026 Fragrances\" is open",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User is on page the button \"Frutarom\" is enabled",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User is on page the button \"Powderpure\" is enabled",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User is on page the button \"Tastepoint by IFF\" is enabled",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "close and quit browser",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Test Touch Page",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to \"https://iff.com\"",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User push the button \"Taste\" the page with title \"Touch – International Flavors \u0026 Fragrances\" is open",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User is on page the text-box \"Peptides Experts\" is enabled",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User push the triengle on this text-box, the second part of text is visible",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User is on page the text-box \"Pioneers of Phospholipids\" is enabled",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User push the triengle on this text-box, the second part of text is visible",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User is on page the text-box \"Formulation Expertise\" is enabled",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User push the triengle on this text-box, the second part of text is visible",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "close and quit browser",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});