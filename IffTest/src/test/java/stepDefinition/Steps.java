package stepDefinition;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class Steps {
	WebDriver driver;
	WebDriverWait wait;
	
	
	@Given("User navigates to {string}")
	public void user_navigates_to(String homepage) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\asadu\\Downloads\\chromedriver.exe");
		driver= new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		wait=new WebDriverWait (driver, 10);
		driver.manage().deleteAllCookies();
		driver.get(homepage);
	}

	@When("User is on home page the title is {string}")
	public void user_is_on_home_page_the_title_is(String expectedTitle) {
		String actualTitle = driver.getTitle();
		Assert.assertEquals(expectedTitle, actualTitle);
	}

	@When("User is on home page the logo is displayed in the top left corner")
	public void user_is_on_home_page_the_logo_is_displayed_in_the_top_left_corner() {
		driver.findElement(By.id("logo"));
	}

	@When("User is on home page the button {string} is enabled")
	public void user_is_on_home_page_the_button_is_enabled(String string) {
		
	}

	@When("User is on home page the button {string} is enabl")
	public void user_is_on_home_page_the_button_is_enabl(String string) {
	}

	@Then("User is on home page the button {string} is enableded")
	public void user_is_on_home_page_the_button_is_enableded(String string) {
	}

	@Then("close and quit browser")
	public void close_and_quit_browser() {
	}

	@When("User push the button {string} the page with title {string} is open")
	public void user_push_the_button_the_page_with_title_is_open(String string, String string2) {
	}

	@When("User is on page the button {string} is enabled")
	public void user_is_on_page_the_button_is_enabled(String string) {
	}

	@Then("User is on page the button {string} is enabled")
	public void user_is_on_page_the_button_is_enabled(String string) {
	}

	@When("User is on page the text-box {string} is enabled")
	public void user_is_on_page_the_text_box_is_enabled(String string) {
	}

	@When("User push the triengle on this text-box, the second part of text is visible")
	public void user_push_the_triengle_on_this_text_box_the_second_part_of_text_is_visible() {
	}

	@Then("User push the triengle on this text-box, the second part of text is visible")
	public void user_push_the_triengle_on_this_text_box_the_second_part_of_text_is_visible() {
	}

}
