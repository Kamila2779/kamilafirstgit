Feature: test IFF website

Scenario: Test Home Page
	Given User navigates to "https://iff.com" 
	When User is on home page the title is "International Flavors & Fragrances"
	When User is on home page the logo with text "IFF" is displayed in the top left corner
	When User is on home page the button "Taste" is enabled
	When User is on home page the button "Touch" is enabled
	When User is on home page the button "Scent" is enabled
	When User is on home page the button "Discover" is enabl
	Then User is on home page the button "Our purpose" is enableded 
	And close and quit browser
	
	Scenario: Test Taste Page
	Given User navigates to "https://iff.com"
	When User push the button "Taste" the page with title "Taste – International Flavors & Fragrances" is open
	When User is on page the button "Frutarom" is enabled
	When User is on page the button "Powderpure" is enabled
	Then User is on page the button "Tastepoint by IFF" is enabled
	And close and quit browser
	
	Scenario: Test Touch Page
	Given User navigates to "https://iff.com" 
	When User push the button "Taste" the page with title "Touch – International Flavors & Fragrances" is open
	When User is on page the text-box "Peptides Experts" is enabled
	When User push the triengle on this text-box, the second part of text is visible
	When User is on page the text-box "Pioneers of Phospholipids" is enabled
	When User push the triengle on this text-box, the second part of text is visible
	When User is on page the text-box "Formulation Expertise" is enabled
	Then User push the triengle on this text-box, the second part of text is visible
	And close and quit browser
	
	
	
