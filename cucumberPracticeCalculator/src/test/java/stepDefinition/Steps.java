package stepDefinition;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class Steps {
	
	WebDriver driver;
	@Given("User navigates to {string} home page is displayed")
	public void user_navigates_to_home_page_is_displayed(String url) {
	   System.setProperty("webdriver.chrome.driver", "C:\\Users\\asadu\\Downloads\\chromedriver.exe");
	   driver = new ChromeDriver();
	   driver.get(url);
		
	}

	@When("User click button {string} expected {string} is displayed")
	public void user_click_button_expected_is_displayed(String buttonValue, String displayedValue) {
		driver.findElement(By.xpath("//input[@type='button' and @value='" + buttonValue +"']")).click();
		String currentValue = driver.findElement(By.xpath("//input[@name='tapefld[]' and @value='" +displayedValue+ "']")).getAttribute("value");
	    System.out.println("Current value ======>"+ currentValue);
	    Assert.assertEquals(buttonValue, currentValue);
	}

	@Then("User click button {string} and sign {string} id displayed and result {string} is dislpayed")
	public void user_click_button_and_sign_id_displayed_and_result_is_dislpayed(String buttonValue, String displayedValue, String expectedResult) {
		driver.findElement(By.xpath("//input[@type='button' and @value='" + buttonValue +"']")).click();
		String currentValue = driver.findElement(By.xpath("//input[@name='tapefld[]' and @value='" +displayedValue+ "']")).getAttribute("value");
		Assert.assertEquals(buttonValue, currentValue);
		System.out.println("Current value ======>"+ currentValue);
		String currentResult = driver.findElement(By.xpath("//input[@name='tapefld[]' and @value='" +expectedResult+ "']")).getAttribute("value");
	    System.out.println("Current result ======>"+ currentResult);
	    Assert.assertEquals(expectedResult, currentResult);

	}

}
