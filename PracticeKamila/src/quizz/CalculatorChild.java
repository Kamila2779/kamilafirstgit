package quizz;

public class CalculatorChild extends MathCalculator {

	public static void main(String[] args) {
		
		CalculatorChild cal = new CalculatorChild();
	
		int summ  = cal.add();
		System.out.println(summ);
		
		int d = cal.div(12, 5);
		System.out.println(d);
		
		System.out.println(cal.multiple(10, 45));
		
		System.out.println(cal.sub(350, 25));
		
	}

}
