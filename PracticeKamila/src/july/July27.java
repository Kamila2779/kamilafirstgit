package july;

public class July27 {

	public static void main(String[] args) {
		
		
		//Naveen training
		
				int a=100;
				int b=200;
				
				String z="Hello";
				String y="World";
				
				double c = 12.33;
				double d = 10.33;
				
				
				System.out.println(a+b);
				
				System.out.println(z+y);
				
				System.out.println(a+b+z+y);
				
				System.out.println(z+y+a+b);
				
				System.out.println(z+y+(a+b));
				
				System.out.println(a+b+z+y+a+z+b+y);
				
				System.out.println(c+d);
				
				System.out.println(z+y+c+d);
				System.out.println("the value of a is "+a);
				
				
				// if-else concept
				int w=10;
				int v=20;
				
				if (v>w){
					System.out.println("v is greater than w");
				}
				else {
					System.out.println("w is greater than v");
				}
				
				// write a logic to find out the highest number
				
				int a1=100;
				int b1=200;
				int c1=300;
				
				//nested if-else
				
				if (a1>b1 & a1>c1) {
					System.out.println("a1 is the highest number");
					}
				else if (b1>a1 & b1>c1) { // y naveena (b1>c1) WHY???????????????????
					System.out.println("b1 is the highest number");
				}
				else {
					System.out.println("c1 is the highest number");
				}
				System.out.println("________________________________________________________________________");
				
				// while loop
				
				int i=1;
				while (i<=10) {
					System.out.print(i);
					i=i+1;
				}
				
				System.out.println("________________________________________________________________________");
				

				// for loop
				for (int j=1; j<=10; j++) {//initialization; conditional; incrementation
					System.out.println(j);
				}
				
				System.out.println("________________________________________________________________________");
				

				// print 10 to 1
				for (int k=10; k>=1; k--) {
					System.out.println(k);
				}
							
				
				System.out.println("________________________________________________________________________");
				
				// print 10 to -10
				for (int k=10; k>=-10; k--) {
					System.out.print(k);
										
				}
				System.out.println("________________________________________________________________________");
				
				// increment-decrement:
				
				int o=1;
				int p=o++;// post increment????????????????????????????????????????//
				System.out.println(o);
				System.out.println(p);
				
				System.out.println("________________________________________________________________________");
				
				int o1=1;
				int p1=++o1;// pre-increment???????????????????????????????????????????
				System.out.println(o1);
				System.out.println(p1);
				
				System.out.println("________________________________________________________________________");
				
				int o2=2;
				int p2= o2--;//post decrement???????????????????????????????????????????
				System.out.println(o2);
				System.out.println(p2);
				
				System.out.println("________________________________________________________________________");
				
				int o3=2;
				int p3=--o3;
				System.out.println(o3);
				System.out.println(p3);
				
				System.out.println("________________________________________________________________________");
				
				//array concept:				
				int t[] = new int [4];
				t[0] = 10;
				t[1] = 20;
				t[2] = 30;
				t[3] = 40;
				System.out.println(t[2]);
				
				int g[] = {10,20,30,40};
				for (int u=0;u<=g.length-1;u++) {
					System.out.println(g[u]);
				}
				
				System.out.println("________________________________________________________________________");
				
				// object array:				
				Object ob[] = {"Tom", "Tesla", "1/1/1990", "London", 1.23};
				for (int l=0; l<=ob.length-1; l++) {
					System.out.println(ob[l]);
				}
				
				
				
				
				
				
				
					
				


	}

}
