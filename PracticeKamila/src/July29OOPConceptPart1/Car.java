package July29OOPConceptPart1;

public class Car extends engine{
	
	//Class vars:
	int mod;
	int wheel;
	

	public static void main(String[] args) {
		
		engine a = new engine();
		color b = new Car();
		Car c = new Car();
		b.green();
		a.green();
		c.green();
	}
	
	public void gear () {
		System.out.println("Car has gear");
	}
	
	public void green () {
		System.out.println("Car is dark green");
	}
	

}
