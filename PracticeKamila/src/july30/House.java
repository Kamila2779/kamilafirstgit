package july30;

public class House {
	
	int bedroom;
	int bathroom;

	public static void main(String[] args) {
	
		
		House a = new House();
		House b = new House();
		House c = new House();
		
		a.bedroom = 3;
		a.bathroom = 3;
		
		b.bedroom = 4;
		b.bathroom = 4;
		
		c.bedroom = 5;
		c.bathroom = 5;
		
		System.out.print(a.bedroom+"/");
		System.out.println(a.bathroom);
		
		System.out.print(b.bedroom+"/");
		System.out.println(b.bathroom);
		
		System.out.print(c.bedroom+"/");
		System.out.println(c.bathroom);
		
		
		System.out.println("After shifting the references:");
		
		a=b;
		b=c;
		c=a;
		
	System.out.println(a.bedroom);
	System.out.println(b.bedroom);
	System.out.println(c.bedroom);
	
	a.bedroom = 10;
	System.out.println(a.bedroom);
	c.bedroom = 15;
	System.out.println(a.bedroom);
	System.out.println(c.bedroom);

	}

}
