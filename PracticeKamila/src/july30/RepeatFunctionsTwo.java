package july30;

public class RepeatFunctionsTwo {

	public static void main(String[] args) {
	
		RepeatFunctionsTwo Two = new RepeatFunctionsTwo();
		Two.love();
		
		int e = Two.population();
		System.out.println(e);
		
		String g = Two.help();
		System.out.println(g);
		
		
		int u = Two.java(45, 5);
		System.out.println(u);
		
		
		
		
	}
	public void love() {
		System.out.println("LOVE");
		
	}
	
	public int population() {
		System.out.print("Population of Arlington - ");
		int a=37000;
		int b=8700;
		int c= a+b;
		return c;
		
	}
	
	public String help() {
		System.out.println("Do you need a help?");
		String r = "Yes, I do.";
		return r;
				
	}
	
	public int java(int c, int d) {
		System.out.println("Java");
		int f = c/d;
		return f;
	}

}
