package TestPackage;

public class ParentClass {
	//to prevent inheritance
	//to prevent method overriding
	
	public void start() {
		System.out.println("Parent class -- start method");
	}

}
