package oopsConceptEncapsulation;

public class ChildClass extends ParentClass{
	

	public static void main(String[] args) {
		
		ChildClass obj = new ChildClass();
		obj.car();
		obj.gun(60);
//		OVERLOADING
		obj.gun("gun");
	}
	
	
	private void gun(int i) {
		System.out.println(i);
	}
	
	private void gun(String b) {
		System.out.println(b);
	}
//	OVERRIDING
	public void car() {
//		int a=4;
//		int f=55;
//		String c="yellow";
//		System.out.println(a + b +c +f);
	}

}
