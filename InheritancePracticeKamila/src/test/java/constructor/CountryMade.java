package constructor;

import carColors.Color;

public class CountryMade extends Color {

    public CountryMade(){
        System.out.println("Constructor: Car made in USA");
    }

    public void CountryMade2(){
        System.out.println("Constructor: Car made in USA");
    }
}
