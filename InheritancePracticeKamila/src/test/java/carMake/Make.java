package carMake;

import constructor.CountryMade;

public class Make {

    public String BMW(){
        String make = "BMW";
        System.out.println("Set car make in parent ==> " + make);
        return make;
    }

    public String Mercedes(){
        String make = "Mercedes";
        System.out.println(make);
        return make;
    }

    public String Audi(){
        String make = "Audi";
        System.out.println("Set car make in parent ==> " + make);
        return make;
    }

}
