package carColors;

import carMake.Make;

public class Color extends Make {

    public String Red(){
        String color = "red";
        System.out.println(color);
        return color;
    }

    public String Green(){
        String color = "green";
        System.out.println(color);
        return color;
    }

    public String Blue(){
        String color = "blue";
        System.out.println(color);
        return color;
    }

}
