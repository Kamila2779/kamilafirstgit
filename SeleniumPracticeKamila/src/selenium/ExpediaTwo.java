package selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class ExpediaTwo {
	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\asadu\\Documents\\PNT\\Soft\\Browser_drivers\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.expedia.com");
		driver.findElement(By.id("tab-flight-tab-hp")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("flight-origin-hp-flight")).sendKeys("Washington");
		driver.findElement(By.xpath("//*[@id=\"aria-option-0\"]/span[2]/div")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("flight-destination-hp-flight")).click();
		driver.findElement(By.id("flight-destination-hp-flight")).sendKeys("Zurich");
		
		driver.findElement(By.xpath("//*[@id=\"aria-option-0\"]")).click();
		System.out.println("-----------------------------------------------------------------------------------------");
		
		WebElement date = driver.findElement(By.xpath("//*[@id=\"flight-departing-hp-flight\"]"));
		String dateVal = "12/20/2019";
		selectDataByJS(date, dateVal);
		
		date = driver.findElement(By.xpath("//*[@id=\"flight-returning-hp-flight\"]"));
		dateVal = "12/29/2019";
		selectDataByJS(date, dateVal);
		
		driver.findElement(By.xpath("//*[@id=\"traveler-selector-hp-flight\"]/div/ul/li/button")).click();
		driver.findElement(By.xpath("//*[@id=\"traveler-selector-hp-flight\"]/div/ul/li/div/div/div/div[1]/div[4]/button")).click();
		driver.findElement(By.xpath("//*[@id=\"traveler-selector-hp-flight\"]/div/ul/li/div/div/div/div[2]/div[1]/div[4]/button")).click();
		driver.findElement(By.xpath("//*[@id=\"traveler-selector-hp-flight\"]/div/ul/li/div/div/div/div[2]/div[1]/div[4]/button")).click();
		try {
		Select select = new Select(driver.findElement(By.xpath("//*[@id=\"flight-age-select-1-hp-flight\"]")));
		select.selectByVisibleText("5");
		select = new Select(driver.findElement(By.xpath("//*[@id=\"flight-age-select-2-hp-flight\"]")));
		select.selectByVisibleText("3");
//		driver.findElement(By.xpath("//*[@id=\"traveler-selector-hp-flight\"]/div/ul/li/div/footer/div/div[2]/button")).click();
		
		driver.findElement(By.xpath("//*[@id=\"traveler-selector-hp-flight\"]/div/ul/li/button")).click();
		driver.findElement(By.xpath("//*[@id=\"gcw-flights-form-hp-flight\"]/div[7]/label/button")).click();
		
		} catch(NoSuchElementException | ElementNotInteractableException e){
			System.out.println("Error");
			e.printStackTrace();
		}
		String expectedTitle = "WAS to ZRH Flights | Expedia";
		String actualTitle = driver.getTitle();
		System.out.println(actualTitle);
		if(actualTitle.equals(expectedTitle)) {
			System.out.println("Success");
			
					}
		else System.out.println("Not success");
		//Thread.sleep(1000);
//		driver.close();
//		driver.quit();
	}
	public static void selectDataByJS(WebElement element, String dateVal) {
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].setAttribute('value', '" + dateVal + "');", element);
		
	}

	
}
