package selenium;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class ExpediaTrTd {

	public static void main(String[] args) throws InterruptedException{
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\asadu\\Documents\\PNT\\Soft\\Browser_drivers\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.navigate().to("https://www.expedia.com/");
		
		Thread.sleep(5000);
		//choose the aiport:
		driver.findElement(By.id("tab-flight-tab-hp")).click();
		driver.findElement(By.id("flight-origin-hp-flight")).click();
		driver.findElement(By.id("flight-origin-hp-flight")).sendKeys("Washington");
		List<WebElement> list = driver.findElements(By.xpath("//ul[@id='typeaheadDataPlain']//li/descendant::div[@class='text']//*[text()='IAD']"));
		for (int i=0; i<list.size(); i++) {
			if (list.get(i).getAttribute("data-value").contains("(IAD) Washington Dulles Intl.")) {
				System.out.println("THE OJFOJ=====>>>>>>"+list.get(i).getText() + " THE ELEMENDT ====>>>>"  + list.get(i));
				list.get(i).click();
				break;
			}
		}
		//choose the destination:
		driver.findElement(By.id("flight-destination-hp-flight")).click();
		driver.findElement(By.id("flight-destination-hp-flight")).sendKeys("Zurich");
		List<WebElement> listReturn = driver.findElements(By.id("typeaheadDataPlain"));
		
		for(int j=0; j<listReturn.size(); j++) {
			System.out.println(listReturn.get(j).getText());
			if(listReturn.get(j).getText().contains("Zurich")) {
				listReturn.get(j).click();
				break;
			}
		}
		//choose date of flight:
		driver.findElement(By.id("flight-departing-hp-flight")).click();
		driver.findElement(By.id("flight-departing-hp-flight")).sendKeys("12/24/2019");
		List<WebElement> dataFlight = driver.findElements(By.xpath("/html/body/meso-native-marquee/section/div/div/div[1]/section/div/div[2]/div[2]/section[1]/form/fieldset[2]/div/div[2]/div/div/div[2]/div[2]"));
		for(int k=0; k<dataFlight.size(); k++) {
//			System.out.println(dataFlight.get(k).getText());
			if(!dataFlight.get(k).getText().contains("Dec 2019")) {
				System.out.println("It is not December");
				driver.findElement(By.xpath("/html/body/meso-native-marquee/section/div/div/div[1]/section/div/div[2]/div[2]/section[1]/form/fieldset[2]/div/div[2]/div/div/div[2]/button[2]")).click();// doesnt print!!!!!!!!!!!!!!!!!!!!!!
			}
		String date = "25-December-2019";
		String dateArr [] = date.split("-");
		String day = dateArr[0];
		String month = dateArr[1];
		String year = dateArr[2];
		
		String beforeXpath = "html/body/meso-native-marquee/section/div/div/div[1]/section/div/div[2]/div[2]/section[1]/form/fieldset[2]/div/div[2]/div/div/div[2]/div[3]/table/tbody/tr[";
		String afterXpath = "]/td[";
		final int totalWeekdays = 7;
		boolean flag = false;
				for (int rowNum=1; rowNum<=5; rowNum++) {
					for (int colNum=1; colNum<=totalWeekdays; colNum++) {
				String dayVal = driver.findElement(By.xpath(beforeXpath+rowNum+afterXpath+colNum+"]/button")).getText();
				System.out.println(dayVal);
				if(dayVal.contains(day)){
					driver.findElement(By.xpath(beforeXpath+rowNum+afterXpath+colNum+"]/button")).click();
					flag = true;
					break;
				}				
			}
					if(flag) {
						break;
					}
		}
		//choose the return date		
		driver.findElement(By.id("flight-returning-hp-flight")).click();
		List<WebElement> dataReturn = driver.findElements(By.xpath("/html/body/meso-native-marquee/section/div/div/div[1]/section/div/div[2]/div[2]/section[1]/form/fieldset[2]/div/div[3]/div/div/div[2]/div[2]/table/caption"));
		for(int l=0; l<dataReturn.size(); l++) {
			if(dataReturn.get(l).getText().contains("Jan 2020")) {
				System.out.println("It is January");// doesnt print!!!!!!!!!!!!!!!!!!!!!!
			}	
				else driver.findElement(By.xpath("/html/body/meso-native-marquee/section/div/div/div[1]/section/div/div[2]/div[2]/section[1]/form/fieldset[2]/div/div[3]/div/div/div[2]/button[2]")).click();
		}
		String dataRet = "10-January-2020";
		String dataRetArr [] = dataRet.split("-");
		String dayRet = dataRetArr[0];
		String monthRet = dataRetArr[1];
		String yearRet = dataRetArr[2];
		//the xpaths have different end. What to put as String afterXpathRet:
//		/html/body/meso-native-marquee/section/div/div/div[1]/section/div/div[2]/div[2]/section[1]/form/fieldset[2]/div/div[3]/div/div/div[2]/div[2]/table/tbody/tr[1]/td[1]
//		/html/body/meso-native-marquee/section/div/div/div[1]/section/div/div[2]/div[2]/section[1]/form/fieldset[2]/div/div[3]/div/div/div[2]/div[2]/table/tbody/tr[3]/td[3]/button
//		/html/body/meso-native-marquee/section/div/div/div[1]/section/div/div[2]/div[2]/section[1]/form/fieldset[2]/div/div[3]/div/div/div[2]/div[2]/table/tbody/tr[2]/td[6]
//		/html/body/meso-native-marquee/section/div/div/div[1]/section/div/div[2]/div[2]/section[1]/form/fieldset[2]/div/div[3]/div/div/div[2]/div[2]/table/tbody/tr[2]/td[7]/button
		String beforeXpathRet = "/html/body/meso-native-marquee/section/div/div/div[1]/section/div/div[2]/div[2]/section[1]/form/fieldset[2]/div/div[3]/div/div/div[2]/div[2]/table/tbody/tr[";
		String afterXpathRet = "]/td[";
		boolean flagRet = false;
		for (int rowNum=1; rowNum<=6; rowNum++) {
			for(int colNum=1; colNum<=totalWeekdays; colNum++) {
				String dayValRet = driver.findElement(By.xpath(beforeXpathRet+rowNum+afterXpathRet+colNum+"]")).getText();
				System.out.println(dayValRet); // why didnt print all days thru 30 jan??? only by 10 jan
				if (dayValRet.contains(dayRet)) {
					driver.findElement(By.xpath(beforeXpathRet+rowNum+afterXpathRet+colNum+"]")).click();
					flagRet = true;
					break;
					}
			}
			if(flagRet) {
				break;
			}
		}
		//choose trevelers:
		driver.findElement(By.xpath("/html/body/meso-native-marquee/section/div/div/div[1]/section/div/div[2]/div[2]/section[1]/form/fieldset[2]/div/div[4]/div/div/ul/li/button")).click();
//		String adult = driver.findElement(By.xpath("/html/body/meso-native-marquee/section/div/div/div[1]/section/div/div[2]/div[2]/section[1]/form/fieldset[2]/div/div[4]/div/div/ul/li/button")).getText();
//		if (adult.contains("2 Adults")) {
//			System.out.println("Two adults choosen");
//		}
		driver.findElement(By.xpath("/html/body/meso-native-marquee/section/div/div/div[1]/section/div/div[2]/div[2]/section[1]/form/fieldset[2]/div/div[4]/div/div/ul/li/div/div/div/div[1]/div[4]/button")).click();
		
		// search:
		driver.findElement(By.xpath("/html/body/meso-native-marquee/section/div/div/div[1]/section/div/div[2]/div[2]/section[1]/form/div[8]/label/button")).click();
		
		// select the flight:	
		driver.findElement(By.xpath("/html/body/div[2]/div[10]/section/div/div[11]/ul/li[1]/div[1]/div[1]/div[2]/div/div[2]/button")).click();
		try {
		driver.findElement(By.xpath("/html/body/div[2]/div[10]/section/div/div[11]/ul/li[1]/div[2]/div/div/div/div[1]/button")).click();
		
		} catch(ElementNotInteractableException e){
			System.out.println("Error");
			e.printStackTrace();
		driver.findElement(By.xpath("/html/body/div[2]/div[10]/section/div/div[11]/ul/li[1]/div[1]/div[1]/div[2]/div/div[2]/button")).click();
		}
		try {
		driver.findElement(By.xpath("/html/body/div[2]/div[10]/section/div/div[11]/ul/li[1]/div[2]/div/div/div/div[1]/button")).click();
		} catch(ElementNotInteractableException | NoSuchElementException e) {
			System.out.println("Error");
			e.printStackTrace();
		}
		}
	}
}



