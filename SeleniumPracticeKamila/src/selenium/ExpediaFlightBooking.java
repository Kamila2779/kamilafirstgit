package selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ExpediaFlightBooking {

	public static void main(String[] args) throws InterruptedException{
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\asadu\\Documents\\PNT\\Soft\\Browser_drivers\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.expedia.com/");
		driver.findElement(By.id("primary-header-flight")).click();
		driver.findElement(By.id("tab-flight-tab-flp")).click();
		driver.findElement(By.id("flight-type-one-way-label-flp")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("flight-origin-flp")).sendKeys("WAS");
		Thread.sleep(15000);
		driver.findElement(By.id("flight-destination-flp")).sendKeys("MILAN"); 
		
		WebElement date = driver.findElement(By.id("flight-departing-single-flp"));
		String dateVal = "30-12-2019";
		selectDataByJS(driver, date, dateVal);
		
		driver.findElement(By.xpath("//*[@id=\"traveler-selector-flp\"]/div/ul/li/button")).click();
		
	//	driver.findElement(By.linkText("Add Adult")).click();
		
		WebElement passenger = driver.findElement(By.id("traveler-selector-flp"));
		String passVal = "3";
		selectPassengersByJS(driver, passenger, passVal);
		
		
		
	
	}
	
	public static void selectDataByJS(WebDriver driver, WebElement element, String dateVal) {
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].setAttribute('value', '"+dateVal+"');", element);
		
	}
	
	public static void selectPassengersByJS(WebDriver driver, WebElement element, String passVal) {
		JavascriptExecutor jsp = (JavascriptExecutor)driver;
		jsp.executeScript("arguments[0].setAttribute('value', '" + passVal + "');", element);
		
	}
}


