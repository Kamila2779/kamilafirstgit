package stepDefinition;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Steps {
	WebDriver driver;

	@Given("User navigates to {string} homepage is displayed")
	public void user_navigates_to_homepage_is_displayed(String string) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\asadu\\Downloads\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(string);
	}

	@When("User click button {string} expected {string} is displayed")
	public void user_click_button_expected_is_displayed(String buttonValue, String displayedValue) {
		driver.findElement(By.xpath("//input[@type='button' and @value='" + buttonValue + "']")).click();
		String currentValue = driver.findElement(By.xpath("//input[@name='tapefld[]' and @value='" + displayedValue + "']")).getAttribute("value");
		System.out.println("currentValue  ==>>  " + currentValue);
		Assert.assertEquals(buttonValue, currentValue);

	}

	@Then("User click button {string} and sign {string} is displayed and number {string} is displayed")
	public void user_click_button_and_sign_is_displayed_and_number_is_displayed(String buttonValue,
			String expectedSign, String expectedResult) {
		driver.findElement(By.xpath("//input[@type='button' and @value='" + buttonValue +"']")).click();
		String currentSign = driver.findElement(By.xpath("//input[@name='tapefld[]' and @value='" + buttonValue + "']")).getAttribute("value");
		Assert.assertEquals(expectedSign, currentSign);
		String currentResult = driver.findElement(By.xpath("//input[@name='tapefld[]' and @value='" + expectedResult + "']")).getAttribute("value");
		Assert.assertEquals(expectedResult, currentResult);
	}
}
