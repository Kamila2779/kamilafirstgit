import org.testng.annotations.Test;

import java.lang.reflect.Array;
import java.util.Arrays;

public class HighestAndLowestValue {
    @Test
    public static void test(){
        int numbers [] = {10, -4, 78, 907, 0, 305};

        int highest = numbers[0];
        int smallest = numbers[0];

        for(int k=1; k<numbers.length; k++){
            if(numbers[k]>highest){
                highest=numbers[k];
            }
            else if (numbers[k]<smallest){
                smallest = numbers[k];

            }
        }
        System.out.println("Given array:"+ Arrays.toString(numbers));
        System.out.println("The highest number is = "+ highest);
        System.out.println("the smallest number is = "+ smallest);

    }


}
