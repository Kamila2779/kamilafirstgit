import org.testng.annotations.Test;

import java.util.HashSet;
import java.util.Set;

public class DuplicateValues {
    @Test
    public static void main (){
        String family [] = {"Airat", "Kamila", "Malik", "Ailin", "Ailin"};

        for (int a=0; a<family.length; a++){
            for (int b = a+1; b<family.length; b++){

                if (family[a].equals(family[b])){
                    System.out.println("This value is duplicate=="+family[b]);
                }
            }
        }

        System.out.println("************************");
        Set<String> store = new HashSet<String>();
        for (String names : family){
            if (store.add(names)==false){
                System.out.println("This value is duplicate=="+names);
            }

        }


    }
}
