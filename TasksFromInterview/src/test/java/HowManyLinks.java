import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.List;

public class HowManyLinks {
    @Test
    public void CountLinks() {
        System.setProperty("webdriver.chrome.driver", "/home/airat/PNT/Soft/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.get("http://iff.com");
        List<WebElement> linklist = driver.findElements(By.tagName("a5"));
        System.out.println(linklist.size());

        for (int i=0;i<linklist.size(); i++){
            String linkText = linklist.get(i).getText();
            System.out.println(linkText);
        }
        driver.close();
    }
}
