package testNGBasics;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestGoogle {
    WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        System.setProperty("webdriver.chrome.driver", "//home//airat//Downloads//chromedriver");
        driver = new ChromeDriver();
        driver.get("http://google.com");
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
    }

    @Test
    public void testTitle(){

       String actualTitle = driver.getTitle();
       Assert.assertEquals(actualTitle, "Google");

    }
    @AfterClass
    public void afterClass(){
        driver.close();
        driver.quit();
    }

}
