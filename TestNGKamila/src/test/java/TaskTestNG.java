import org.testng.annotations.*;

public class TaskTestNG {

    @BeforeMethod
    public void beforeMethod(){
        System.out.println("I am @beforeMethod");
    }
    @BeforeClass
    public void beforeClass(){
        System.out.println("I am @beforeClass");
    }
    @BeforeTest
    public void beforeTest(){
        System.out.println("I am @beforeTest");
    }
    @BeforeSuite
    public void beforeSuite(){
        System.out.println("I am @beforeSuite");
    }
    @Test
    public void testOne(){
        System.out.println("I am @testOne1");
    }
    public void testone(){
        System.out.println("I am @testOne2");
    }
    public void TestOne(){
        System.out.println("I am @testOne3");
    }
    @Test
    public void testTwo(){
        System.out.println("I am @testTwo");
    }
    @Test
    public void testTree(){
        System.out.println("I am @TestTree");
    }

    @Test
    public void testFour(){
        System.out.println("I am @TestFour");
    }
    @AfterMethod
    public void afterMethod(){
        System.out.println("I am @afterMethod");
    }
    @AfterClass
    public void afterClass(){
        System.out.println("I am @afterMethod");
    }
    @AfterSuite
    public void afterSuite(){
        System.out.println("I am @afterSuite");
    }
    @AfterTest
    public void afterTest(){
        System.out.println("I am @afterTest");
    }


}
