package stepDefinition;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class Steps {
	WebDriver driver;
	WebDriverWait wait;

	@Given("User already on login page")
	public void user_already_on_login_page() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\asadu\\Downloads\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 10);
		driver.get("http://facebook.com");

	}

	@When("Title of page is {string}")
	public void title_of_page_is(String titleExpected) {
		String titleActual = driver.getTitle();
		System.out.println("Actual title is ====>"+ titleActual);
		Assert.assertEquals(titleExpected, titleActual);
	}

	@Then("User enters {string} and {string}")
	public void user_enters_and(String username, String password) {
		driver.findElement(By.xpath("//input[@name='email']")).sendKeys(username);
		driver.findElement(By.xpath("//input[@name='pass']")).sendKeys(password);
	}

	@Then("User clicks submit button")
	public void user_clicks_submit_button() {
		driver.findElement(By.xpath("//*[@id=\"u_0_b\"]")).click();
	}

	@Then("User on home page {string}")
	public void user_on_home_page(String expectedTitle) {
		String actualTitle= driver.getTitle();
		System.out.println("Title=====>" + actualTitle);
	}
	@Then("User closes browser")
	public void user_closes_browser() {
	   driver.close();
	   driver.quit();
	}

}
