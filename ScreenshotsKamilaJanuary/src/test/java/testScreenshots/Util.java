package testScreenshots;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;

public class Util {

    public WebDriver setDriver() {
            System.setProperty("webdriver.chrome.driver", "/home/airat/PNT/Soft/chromedriver");
            WebDriver driver = new ChromeDriver();
            return driver;
    }


    public void takeScreenshot(String fileName, WebDriver driver) throws IOException {

        File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(srcFile, new File("/home/airat/IdeaProject/ScreenshotsKamilaJanuary/src/test/java/Screenshots/"
                +fileName+System.currentTimeMillis()+".png"));

    }

}
