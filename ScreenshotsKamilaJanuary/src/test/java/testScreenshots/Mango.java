package testScreenshots;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

public class Mango extends Util {
    WebDriver driver;
    Util util;

    @BeforeMethod
    public void setVariables() {
        util = new Util();
        driver = util.setDriver();
        driver.get("http://mango.com");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
    }

    @Test
    public void testTitle() throws IOException {
        util.takeScreenshot(driver.getTitle(), driver);

    }
    @AfterMethod
    public void afterMethod(){
        driver.close();
        driver.quit();
    }

}
