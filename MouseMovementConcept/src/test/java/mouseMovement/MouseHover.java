package mouseMovement;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

public class MouseHover {

    @Test
    public void openBrowser() throws IOException {

        System.setProperty("webdriver.chrome.driver", "/home/airat/PNT/Soft/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.get("http://mango.com");
        driver.findElement(By.xpath("//div[@id=\"lang_US\"]//a[@tabindex=\"4\" ]")).click();


        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(By.id("she"))).build().perform();
        File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(srcFile, new File("/home/airat/IdeaProject/MouseMovementConcept/src/test/java/screenshots/"
                +System.currentTimeMillis()+".png"));

        driver.close();
        driver.quit();

    }

}
