package ifcondition;

public class IfCondition {

    public static void main(String[] args) {
        int x = 10;
//    Check if x less than 10
        if (x < 10) {
            System.out.println("x<10");
        } else if (x > 10) {
            System.out.println("x>10");
        } else {
            System.out.println("x=10");
        }
    }

}
