$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/Features.feature");
formatter.feature({
  "name": "Test google",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Test Search Button",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Smoke"
    }
  ]
});
formatter.step({
  "name": "User navigates to \"https://www.google.com\" home page is displayed",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.user_navigates_to_home_page_is_displayed(String)"
});
formatter.result({
  "error_message": "org.openqa.selenium.WebDriverException: unknown error: cannot find Chrome binary\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027DESKTOP-T9V106E\u0027, ip: \u0027192.168.56.1\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_201\u0027\nDriver info: driver.version: ChromeDriver\nremote stacktrace: Backtrace:\n\tOrdinal0 [0x0050A903+1550595]\n\tOrdinal0 [0x0048A701+1025793]\n\tOrdinal0 [0x0040C6E5+509669]\n\tOrdinal0 [0x0039C082+49282]\n\tOrdinal0 [0x003BACE7+175335]\n\tOrdinal0 [0x003BA8ED+174317]\n\tOrdinal0 [0x003B8CDB+167131]\n\tOrdinal0 [0x003A144A+70730]\n\tOrdinal0 [0x003A24D0+74960]\n\tOrdinal0 [0x003A2469+74857]\n\tOrdinal0 [0x004A42C7+1131207]\n\tGetHandleVerifier [0x005A70FD+523789]\n\tGetHandleVerifier [0x005A6E90+523168]\n\tGetHandleVerifier [0x005AE1E7+552695]\n\tGetHandleVerifier [0x005A78FA+525834]\n\tOrdinal0 [0x0049B7FC+1095676]\n\tOrdinal0 [0x004A633B+1139515]\n\tOrdinal0 [0x004A64A3+1139875]\n\tOrdinal0 [0x004A5425+1135653]\n\tBaseThreadInitThunk [0x76F40419+25]\n\tRtlGetAppContainerNamedObjectPath [0x77AF662D+237]\n\tRtlGetAppContainerNamedObjectPath [0x77AF65FD+189]\n\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.W3CHandshakeResponse.lambda$errorHandler$0(W3CHandshakeResponse.java:62)\r\n\tat org.openqa.selenium.remote.HandshakeResponse.lambda$getResponseFunction$0(HandshakeResponse.java:30)\r\n\tat org.openqa.selenium.remote.ProtocolHandshake.lambda$createSession$0(ProtocolHandshake.java:126)\r\n\tat java.util.stream.ReferencePipeline$3$1.accept(ReferencePipeline.java:193)\r\n\tat java.util.Spliterators$ArraySpliterator.tryAdvance(Spliterators.java:958)\r\n\tat java.util.stream.ReferencePipeline.forEachWithCancel(ReferencePipeline.java:126)\r\n\tat java.util.stream.AbstractPipeline.copyIntoWithCancel(AbstractPipeline.java:498)\r\n\tat java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:485)\r\n\tat java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:471)\r\n\tat java.util.stream.FindOps$FindOp.evaluateSequential(FindOps.java:152)\r\n\tat java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)\r\n\tat java.util.stream.ReferencePipeline.findFirst(ReferencePipeline.java:464)\r\n\tat org.openqa.selenium.remote.ProtocolHandshake.createSession(ProtocolHandshake.java:128)\r\n\tat org.openqa.selenium.remote.ProtocolHandshake.createSession(ProtocolHandshake.java:74)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:136)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.startSession(RemoteWebDriver.java:213)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.\u003cinit\u003e(RemoteWebDriver.java:131)\r\n\tat org.openqa.selenium.chrome.ChromeDriver.\u003cinit\u003e(ChromeDriver.java:181)\r\n\tat org.openqa.selenium.chrome.ChromeDriver.\u003cinit\u003e(ChromeDriver.java:168)\r\n\tat org.openqa.selenium.chrome.ChromeDriver.\u003cinit\u003e(ChromeDriver.java:123)\r\n\tat stepDefinition.Steps.user_navigates_to_home_page_is_displayed(Steps.java:28)\r\n\tat ✽.User navigates to \"https://www.google.com\" home page is displayed(file:src/test/java/Features.feature:4)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "User navigate to homepage, search field is displayed",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_navigate_to_homepage_search_field_is_displayed()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters word \"potato\" and clicks button \"Google search\"",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.user_enters_word_and_clicks_button(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Page \"results \"with results is displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.page_with_results_is_displayed(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Close and quit browser",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.close_and_quit_browser()"
});
formatter.result({
  "status": "skipped"
});
});