package runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

	@RunWith(Cucumber.class)
	@CucumberOptions(features="src/test/java/Features.feature", glue="stepDefinition", plugin = {"pretty","html:Report"},
	monochrome = true, dryRun = false, strict = false, tags = {"@Smoke"})
//how to find plugin
public class TestRunner {
	

}
