package stepDefinition;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.core.api.Scenario;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Steps {
	WebDriver driver;
	WebDriverWait wait;


    
	@Given("User navigates to {string} home page is displayed")
	public void user_navigates_to_home_page_is_displayed(String homepage) {
	    System.setProperty("webdriver.chrome.driver", "C:\\Users\\asadu\\Downloads\\chromedriver.exe");
	    driver = new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	    wait = new WebDriverWait(driver, 10);
	    driver.get(homepage);
	   	}
	

	@When("User navigate to homepage, search field is displayed")
	public void user_navigate_to_homepage_search_field_is_displayed (){
		boolean b = driver.findElement(By.xpath("//input[@name='q' and @role='combobox']")).isDisplayed();
		System.out.println("SearchField is displayed: " +b);	
		Assert.assertEquals(true, b);
	}

	@When("User enters word {string} and clicks button {string}")
	public void user_enters_word_and_clicks_button(String searchWord, String searchButton) {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='q' and @role='combobox']")));
		driver.findElement(By.xpath("//input[@name='q' and @role='combobox']")).sendKeys(searchWord);
		//wait.until(ExpectedConditions.textToBePresentInElementValue(By.xpath("//input[@class='RNmpXc']//preceding-sibling::input[@class='gNO89b']"), "Google Search"));
		driver.findElement(By.xpath("//input[@class='RNmpXc']//preceding-sibling::input[@class='gNO89b']")).click();
		
		
	}

	@Then("Page {string}with results is displayed")
	public void page_with_results_is_displayed(String expectedTitle) {
		String currentTitle = driver.getTitle();
		System.out.println(currentTitle);
		try {
		Assert.assertEquals(expectedTitle, currentTitle);
		} catch(ComparisonFailure e) {
			
			e.printStackTrace();
		}
	}
	
	@Then("Close and quit browser")
	public void close_and_quit_browser() {
		driver.close();
		driver.quit();
	}
	
	@When("User navigates to homepage, {string} button is enabled")
	public void user_navigates_to_homepage_button_is_enabled(String buttonName) {
		boolean b1 = driver.findElement(By.linkText(buttonName)).isEnabled();
		Assert.assertEquals(true, b1);   
	}

	@When("User click {string} button the page with text {string} is displayed")
	public void user_click_button_the_page_with_text_is_displayed(String signInButton, String textOnPage) {
		driver.findElement(By.linkText(signInButton)).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//parent::div[@class='Y4dIwd']//span"), textOnPage));
		System.out.println(driver.getTitle());
		boolean b2 = driver.findElement(By.xpath("//parent::div[@class='Y4dIwd']//span")).isDisplayed();
		Assert.assertEquals(true, b2);
	}
	@When("User input {string} to Email field and push the button {string} the {string} is displayed")
		public void user_input_to_Email_field_and_push_the_button_the_is_displayed(String realEmail, String buttonNext, String passwordField) {
		 driver.findElement(By.xpath("//input[@type='email']")).sendKeys(realEmail);
		 driver.findElement(By.xpath("//span[@class= 'RveJvd snByac']")).click();
		 wait.until(ExpectedConditions.titleContains("Sign in - Google Accounts"));
		 boolean b3 = driver.findElement(By.xpath("//input[@name='"+passwordField+"']")).isDisplayed();
		try {
			Assert.assertEquals(true, b3);
		} catch(AssertionError f) {
			
			f.printStackTrace();
		}
	}

	@When("User input to the field {string} text {string} and push the button {string}")
	public void user_input_to_the_field_text_and_push_the_button(String passwordField, String password,
			String buttonNext1) {
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);
		//driver.findElement(By.linkText(buttonNext1)).click();
		driver.findElement(By
				.xpath("//parent::div[@id='passwordNext']//parent::span[@class='CwaK9']//span[@class='RveJvd snByac']"))
				.click();
		wait.until(ExpectedConditions.titleContains("Sign in - Google Accounts"));

	}

	@Then("Page with title {string} is displayed")
	public void page_with_title_is_displayed(String expectedTitle) {
		//wait.until(ExpectedConditions.titleContains("Google Account: Kamila Aibedullova));
		String currentTitle = driver.getTitle();
		System.out.println("CurrentTitle===="+currentTitle);
		Assert.assertEquals(expectedTitle, currentTitle);

	}
}
